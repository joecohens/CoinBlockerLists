![img](img/CoinBL_Logo.png)
## Simple lists that can help prevent cryptomining in the browser or other applications.

[![pipeline status](https://gitlab.com/ZeroDot1/CoinBlockerLists/badges/master/pipeline.svg)](https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master)
[![Follow on Twitter](https://img.shields.io/twitter/follow/zero_dot1.svg?logo=twitter)](https://twitter.com/zero_dot1)
<img src="http://img.shields.io/liberapay/patrons/ZeroDot1.svg?logo=liberapay">

# Downloads
## [All downloads can be found on the CoinBlockerLists Homepage](https://zerodot1.gitlab.io/CoinBlockerListsWeb/downloads.html)

# Donations
Every donation helps me to continue the work on the CoinBlockerLists.
Donations are possible via the following options:
- [Donate](https://smile.amazon.de/hz/wishlist/ls/2DDEDPJU2996I/)

Every donation is much appreciated.
Many thanks to all supporters!


## RSS Feed [https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master?format=atom](https://gitlab.com/ZeroDot1/CoinBlockerLists/commits/master?format=atom)

# Check for false positives
- Note: With this tool you can check the CoinBlockerLists for false positives.
- If a domain is not in the CoinBlockerLists you want to add please contact me via email:  [zerodot1@bk.ru](mailto:zerodot1@bk.ru)
- https://malware-research.org/coinblockerlists/

Many thanks to [GelosSnake](https://twitter.com/GelosSnake) :)

### CoinBlockerLists is now listed in FireHOL: [http://iplists.firehol.org/](http://iplists.firehol.org/)  
- [http://iplists.firehol.org/?ipset=coinbl_hosts](http://iplists.firehol.org/?ipset=coinbl_hosts)
- [http://iplists.firehol.org/?ipset=coinbl_hosts_optional](http://iplists.firehol.org/?ipset=coinbl_hosts_optional)
- [http://iplists.firehol.org/?ipset=coinbl_hosts_browser](http://iplists.firehol.org/?ipset=coinbl_hosts_browser)
- [http://iplists.firehol.org/?ipset=coinbl_ips](http://iplists.firehol.org/?ipset=coinbl_ips)

Many thanks to [https://github.com/ktsaou](https://github.com/ktsaou) :)

# More information about the project can be found on the CoinBlockerLists homepage: [https://zerodot1.gitlab.io/CoinBlockerListsWeb/](https://zerodot1.gitlab.io/CoinBlockerListsWeb/index.html)

# Contributing
You can contribute to the project as you want and you can also fork it but the changes must be public as the GPLv3 license specifies.
Thanks to all who use and support CoinBlockerLists.
[Issues](https://gitlab.com/ZeroDot1/CoinBlockerLists/issues/new) [Merge Requests](https://gitlab.com/ZeroDot1/CoinBlockerLists/merge_requests/new)

## References: [https://zerodot1.gitlab.io/CoinBlockerListsWeb/references.html](https://zerodot1.gitlab.io/CoinBlockerListsWeb/references.html)

## Partners
----------------------------------------------------------------------------------------------------------------
[![img](https://zerodot1.gitlab.io/CoinBlockerListsWeb/img/Deteque_Logo.png)](https://www.deteque.com/#)
### [Take a look at Deteque's great products.](https://www.deteque.com/#)
----------------------------------------------------------------------------------------------------------------
[![img](https://zerodot1.gitlab.io/CoinBlockerListsWeb/img/STLogo3.png)](https://securitytrails.com/)
### [DNS history, IP info and company information by SecurityTrails.](https://securitytrails.com/)
----------------------------------------------------------------------------------------------------------------
### Contact
You have a problem or want to change something in the CoinBlockerLists, please contact me. The following contact options are available.
- Twitter: [@zero_dot1](https://twitter.com/zero_dot1)
- Mastodon: [@katze](https://mastodon.social/@katze)
- E-Mail: [ZeroDot1@bK.Ru](mailto:zerodot1@bk.ru)
- Skype [This email address is only for Skype Chats and is NOT email support!]: [zerodot1@outlook.com](mailto:zerodot1@outlook.com)
